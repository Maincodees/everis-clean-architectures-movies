package es.maincode.data.mappers

import es.maincode.data.entities.Movie
import es.maincode.data.entities.MovieResponse
import es.maincode.domain.models.MovieBO
import es.maincode.domain.models.MovieResponseBO

class MovieMapper {

    fun transform(movieResponse: MovieResponse?): MovieResponseBO {
        val movieResponseBO = MovieResponseBO()
        return movieResponseBO.apply {
            page = movieResponse?.page
            results = transform(movieResponse?.results)
            total_pages = movieResponse?.total_pages
            total_results = movieResponse?.total_results
        }
    }

    private fun transform(movie: Movie?): MovieBO {
        val movieBO = MovieBO()
        return movieBO.apply {
            adult = movie?.adult
            backdrop_path = movie?.backdrop_path
            genre_ids = movie?.genre_ids
            id = movie?.id
            original_language = movie?.original_language
            original_title = movie?.original_title
            overview = movie?.overview
            popularity = movie?.popularity
            poster_path = movie?.poster_path
            release_date = movie?.release_date
            title = movie?.title
            video = movie?.video
            vote_average = movie?.vote_average
            vote_count = movie?.vote_count
        }
    }

    private fun transform(movieList: List<Movie>?): List<MovieBO>? {
        return movieList?.map { transform(it) }
    }
}