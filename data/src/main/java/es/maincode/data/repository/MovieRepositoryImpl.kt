package es.maincode.data.repository

import android.app.Application
import es.maincode.common.Either
import es.maincode.data.mappers.MovieMapper
import es.maincode.data.MovieServiceManager
import es.maincode.data.R
import es.maincode.domain.repositories.MovieRepository
import es.maincode.domain.models.MovieResponseBO

class MovieRepositoryImpl(application: Application?, val mapper: MovieMapper):
    MovieRepository {

    private val apiKey = application?.getString(R.string.key_api_movie)
    private val regionRepository = "US"

    override suspend fun getListMoview(): Either<Any, MovieResponseBO> {
        return MovieServiceManager.service.getlistPopularMoviesAsync(apiKey, regionRepository).let {
            if (it.isSuccessful) Either.Right(mapper.transform(it.body())) else Either.Left("ERROR")
        }
    }
}