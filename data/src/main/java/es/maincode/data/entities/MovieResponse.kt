package es.maincode.data.entities

import es.maincode.data.entities.Movie

data class MovieResponse(
    val page: Int,
    val results: List<Movie>,
    val total_pages: Int,
    val total_results: Int
)