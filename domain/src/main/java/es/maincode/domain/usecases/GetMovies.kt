package es.maincode.domain.usecases

import es.maincode.common.Either
import es.maincode.domain.repositories.MovieRepository
import es.maincode.domain.models.MovieResponseBO

class GetMovies(private val movieRepository: MovieRepository) {
    suspend fun invoke(): Either<Any, MovieResponseBO> = movieRepository.getListMoview()
}