package es.maincode.domain.models

import es.maincode.domain.models.MovieBO

class MovieResponseBO(
    var page: Int? = null,
    var results: List<MovieBO>? = null,
    var total_pages: Int? = null,
    var total_results: Int? = null
)