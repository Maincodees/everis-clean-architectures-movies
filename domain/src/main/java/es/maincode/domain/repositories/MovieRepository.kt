package es.maincode.domain.repositories

import es.maincode.common.Either
import es.maincode.domain.models.MovieResponseBO

interface MovieRepository {
    suspend fun getListMoview(): Either<Any, MovieResponseBO>
}