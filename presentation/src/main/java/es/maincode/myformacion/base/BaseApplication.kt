package es.maincode.myformacion.base

import android.app.Application
import es.maincode.myformacion.di.KoinConfiguration
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BaseApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@BaseApplication)
            modules(
                listOf(KoinConfiguration().getModule())
            )
        }
    }
}