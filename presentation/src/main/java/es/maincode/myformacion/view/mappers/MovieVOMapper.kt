package es.maincode.myformacion.view.mappers

import es.maincode.domain.models.MovieBO
import es.maincode.domain.models.MovieResponseBO
import es.maincode.myformacion.view.models.MovieResponseVO
import es.maincode.myformacion.view.models.MovieVO

class MovieVOMapper {
    fun transform(movieResponseBO: MovieResponseBO): MovieResponseVO {
        val movieResponseVO = MovieResponseVO()
        return movieResponseVO.apply {
            page = movieResponseBO.page
            total_pages = movieResponseBO.total_pages
            total_results = movieResponseBO.total_results
            results = transform(movieResponseBO.results)
        }
    }

    private fun transform(movieListBO: List<MovieBO>?): List<MovieVO>? {
        return movieListBO?.map { transform(it) }
    }

    private fun transform(movieBO: MovieBO): MovieVO {
        return MovieVO().apply {
            poster_path = movieBO.poster_path
            title = movieBO.title
        }
    }
}