package es.maincode.myformacion.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import es.maincode.myformacion.R
import es.maincode.myformacion.view.fragments.MainFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().replace(R.id.main_content, MainFragment(), "MainFragment").commit()
    }
}
