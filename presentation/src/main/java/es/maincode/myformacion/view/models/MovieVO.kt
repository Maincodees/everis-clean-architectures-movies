package es.maincode.myformacion.view.models

import java.io.Serializable

class MovieVO (
    var poster_path: String? = null,
    var title: String? = null
): Serializable