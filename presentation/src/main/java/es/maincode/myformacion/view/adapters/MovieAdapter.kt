package es.maincode.myformacion.view.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import es.maincode.common.inflate
import es.maincode.common.loadUrl
import es.maincode.myformacion.R
import es.maincode.myformacion.view.models.MovieVO
import kotlinx.android.synthetic.main.view_movie.view.*

class MovieAdapter(private val listener: (MovieVO) -> Unit) : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    var movies: List<MovieVO> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.inflate(R.layout.view_movie, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = movies.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = movies[position]
        holder.bind(movie)
          holder.itemView.setOnClickListener { listener(movie) }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(movie: MovieVO) {
            itemView.movieTitle.text = movie.title
            itemView.movieCover.loadUrl("https://image.tmdb.org/t/p/w185/${movie.poster_path}")
        }
    }

}