package es.maincode.myformacion.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.squareup.picasso.Picasso
import es.maincode.myformacion.R
import es.maincode.myformacion.view.models.MovieVO
import kotlinx.android.synthetic.main.fragment_detail.*

class MovieDetailFragment : Fragment() {

    private var movie: MovieVO? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            it.getSerializable("movie").let { movieVO ->
                movie = movieVO as MovieVO
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        movie?.let { movieVO ->
            Picasso.get().load("https://image.tmdb.org/t/p/w185/${movieVO.poster_path}").fit().centerCrop().into(iv_poster)
            tv_title?.let { it.text = movieVO.title }
        }
    }
}