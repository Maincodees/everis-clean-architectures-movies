package es.maincode.myformacion.view.models

class MovieResponseVO (
    var page: Int? = null,
    var results: List<MovieVO>? = null,
    var total_pages: Int? = null,
    var total_results: Int? = null
)