package es.maincode.myformacion.view.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import es.maincode.domain.usecases.GetMovies
import es.maincode.domain.models.MovieResponseBO
import es.maincode.myformacion.view.mappers.MovieVOMapper
import es.maincode.myformacion.view.models.MovieVO
import es.maincode.myformacion.view.sealed.ViewState
import kotlinx.coroutines.launch

class MovieViewModel(private val getMovies: GetMovies, private val mapper: MovieVOMapper) :
    ViewModel() {

    private val _state = MutableLiveData<ViewState>()
    val state: LiveData<ViewState>
        get() {
            if (_state.value == null) refresh()
            return _state
        }

    private fun refresh() {
        viewModelScope.launch {
            _state.value = ViewState.Loading
            getMovies.invoke().fold(::ShowError, ::ShowMovies)
        }
    }

    private fun ShowMovies(movieResponseBO: MovieResponseBO) {
        _state.value = ViewState.ShowList(mapper.transform(movieResponseBO))
    }

    private fun ShowError(any: Any) {
        _state.value = ViewState.ShowError(any)
    }


    fun onClickMovie(movie: MovieVO) {
        _state.value = ViewState.Navegation(movie)
    }
}


