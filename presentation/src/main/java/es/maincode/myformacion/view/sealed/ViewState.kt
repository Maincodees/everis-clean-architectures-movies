package es.maincode.myformacion.view.sealed

import es.maincode.myformacion.view.models.MovieResponseVO
import es.maincode.myformacion.view.models.MovieVO

sealed class ViewState {
    object Loading : ViewState()
    class ShowList(val movies: MovieResponseVO): ViewState()
    class Navegation (val movie: MovieVO) : ViewState()
    class ShowError(any: Any): ViewState()
}