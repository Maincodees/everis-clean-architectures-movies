package es.maincode.myformacion.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import es.maincode.myformacion.R
import es.maincode.myformacion.view.adapters.MovieAdapter
import es.maincode.myformacion.view.sealed.ViewState
import es.maincode.myformacion.view.viewmodels.MovieViewModel
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment: Fragment() {


    private val viewModel  by viewModel <MovieViewModel>()
    private lateinit var adapter: MovieAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initList()
        initObserver()
    }

    private fun initList() {
        adapter = MovieAdapter(viewModel::onClickMovie)
        rvMovies?.adapter = adapter
    }

    private fun initObserver() {
        viewModel.state.observe(this, Observer  (::updateList) )
    }

    private fun updateList(state: ViewState) {
        progress.visibility = if (state is ViewState.Loading) View.VISIBLE else View.GONE

        when(state){
            is ViewState.ShowList -> {
                state.movies.results?.let { adapter.movies = it }
                adapter.notifyDataSetChanged()
            }
            is ViewState.Navegation -> {
                val bundle = Bundle()
                bundle.putSerializable("movie", state.movie)
                val fragment = MovieDetailFragment()
                fragment.arguments = bundle
                fragmentManager?.beginTransaction()?.add(R.id.main_content, fragment, "movieDetail")?.commit()
            }
        }
    }






}