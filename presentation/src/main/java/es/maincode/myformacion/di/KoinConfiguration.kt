package es.maincode.myformacion.di

import es.maincode.data.mappers.MovieMapper
import es.maincode.data.repository.MovieRepositoryImpl
import es.maincode.domain.usecases.GetMovies
import es.maincode.domain.repositories.MovieRepository
import es.maincode.myformacion.view.mappers.MovieVOMapper
import es.maincode.myformacion.view.viewmodels.MovieViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

class KoinConfiguration {
    fun getModule() = module {
        factory { MovieVOMapper() }
        factory { GetMovies(get()) }
        factory { MovieMapper() }
        factory<MovieRepository> { MovieRepositoryImpl(androidApplication(), get()) }
        viewModel { MovieViewModel(get(), get()) }
    }
}